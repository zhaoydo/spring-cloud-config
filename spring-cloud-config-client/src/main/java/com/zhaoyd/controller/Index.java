package com.zhaoyd.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaoyd
 * @date 2018/8/5
 */
@RestController
public class Index {
    @Value("${hello}")
    private String hello;
    @RequestMapping("index")
    public String index(){

        return hello;
    }
}
